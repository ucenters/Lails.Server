﻿using Autofac;
using Lails.Log;
using System;
using Owin;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Threading;
using System.Web.Http.Controllers;

namespace Lails.Server.Demo
{
    
    class Program
    {
        static void Main(string[] args)
        {
            //初始化前加入
            Filters.Add(new TestFilter());

            AppServer.Run(AppConfig.Default, () =>
            {
                Config config = AppConfig.Load<Config>();
                AppLogger.Info("Server Start.");

                #region 调试模式下给Swagger附加参数

                SwaggerAttachParams.Add("appId", "header");

                #endregion

                //使用OwinContext 中间件

                MiddlewareExtensions.Use(app => { app.Use<OwinContextMiddleware>(); });

                //注入

                AutofacConfig.Builder.Register<string>(c =>
                {
                    return OwinContext.Current.Request.Headers.Get("appId") ?? string.Empty;
                }).As<string>().InstancePerRequest();


                AutofacConfig.Builder.RegisterType(typeof(Logger)).SingleInstance();
                //AutofacConfig.Builder.Register((c) => new Dispatcher((CacheService)c.ResolveOptional(typeof(CacheService)))).SingleInstance();


            }, () =>
            {
                AppLogger.Info("Server Stop.");
            });
        }
    }
}
