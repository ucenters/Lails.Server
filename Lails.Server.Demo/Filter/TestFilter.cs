﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;

namespace Lails.Server.Demo
{
    /// <summary>
    /// 
    /// </summary>
    public class TestFilter : System.Web.Http.Filters.IActionFilter
    {
        /// <summary>
        /// 
        /// </summary>
        public bool AllowMultiple => throw new NotImplementedException();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="actionContext"></param>
        /// <param name="cancellationToken"></param>
        /// <param name="continuation"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            return continuation.Invoke();
        }
    }
}
