﻿using Lails.Log;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lails.Server
{
    public class AppLogger
    {
        static Logger _logger;

        static AppLogger()
        {
            _logger = new Logger();
        }

        public static void Info(string message)
        {
            _logger.Info(message);
        }
    }
}
